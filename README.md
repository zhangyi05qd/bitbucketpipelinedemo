## What Continuous Integrations (bitbucket-pipelines.yml) do
### Build & Test
- Load image (Linux)
- Install necessary packages (e.g. sfdx cli)
- Decrypt server key
- Authorize dev hub Using the JWT Bearer Flow
- Create scratch org, push source & data
- Deploy other metadata using mdapi (optional)
- Run build test
- Destroy org
### Deploy to Sandbox (DEV, SIT, UAT)
- Load image (Linux)
- Install necessary packages (e.g. sfdx cli)
- Decrypt server key
- Authorize Sandbox Using the JWT Bearer Flow
- Deploy to Sandbox using source:deploy
- Deploy other metadata using mdapi (optional)
### Notes
- Can prepare image with packages pre-installed (Reference: https://blog.enree.co/2019/06/setting-up-sfdx-continuous-integration-using-bitbucket-pipelines-with-docker-image.html)
- Atlassian doc on bitbucket-pipelines.yml https://support.atlassian.com/bitbucket-cloud/docs/configure-bitbucket-pipelinesyml/


## Steps to get Pipeline running
### 0. Enable Dev Hub
To use scratch org, Dev Hub feature needs to be enabled.

Go to Prod environment - Setup - Dev Hub - Enable Dev Hub.
### 1. Create a Private Key and Self-Signed Digital Certificate 
Reference: https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_key_and_cert.htm

The OAuth 2.0 JWTbearer authorization flow requires a digital certificate and the private key used to sign the certificate. You upload the digital certificate to the custom connected app that is also required for the JWT bearer authorization flow. You can use your own private key and certificate issued by a certification authority. Alternatively, you can use OpenSSL to create a key and a self-signed digital certificate.
This process produces two files.

- `server.key` - The private key. You specify this file when you authorize an org with the auth:jwt:grant command.

- `server.crt` - The digital certification. You upload this file when you create the connected app required by the JWT bearer flow.

1. If necessary, install OpenSSL on your computer.
To check whether OpenSSL is installed on your computer, run this command.
```
which openssl
```
2. In Terminal or a Windows command prompt, create a directory to store the generated files, and change to the directory.
```
mkdir /Users/jdoe/JWT
```
```
cd /Users/jdoe/JWT
```
3. Generate a private key, and store it in a file called `server.key`.
```
openssl genrsa -des3 -passout pass:SomePassword -out server.pass.key 2048
```
```
openssl rsa -passin pass:SomePassword -in server.pass.key -out server.key
```
You can delete the `server.pass.key` file because you no longer need it.

4. Generate a certificate signing request using the `server.key` file. Store the certificate signing request in a file called `server.csr`. Enter information about your company when prompted.
```
openssl req -new -key server.key -out server.csr
```
5. Generate a self-signed digital certificate from the `server.key` and `server.csr` files. Store the certificate in a file called `server.crt`.
```
openssl x509 -req -sha256 -days 365 -in server.csr -signkey server.key -out server.crt
```
As `server.key` will be stored in the repository, it is necessary to protect it with encryption.

6. Run below command to get the Key and Initialization Vector.
```
openssl enc -nosalt -aes-256-cbc -in server.key -out server.key.enc -base64 -pass pass:PasswordOnlyYouKnow -P
```
> Make note of the key and iv from the output.
Run the command without `-P` switch to get encrypted file `server.key.enc`
```
openssl enc -nosalt -aes-256-cbc -in server.key -out server.key.enc -base64 -pass pass:PasswordOnlyYouKnow
```


### 2. Create Connected App

Reference: https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_connected_app.htm

Setup Connected App in Dev Hub org and each sandbox org (e.g. DEV, SIT, UAT).

1. Log in to your Dev Hub / DEV / SIT / UAT org.
2. From Setup, enter App Manager in the Quick Find box to get to the Lightning Experience App Manager.
3. In the top-right corner, click New Connected App.
4. Update the basic information as needed, such as the connected app name and your email address.
5. Select Enable OAuth Settings.
6. For the callback URL, enter `http://localhost:1717/OauthRedirect`.

     If port 1717 (the default) is already in use on your local machine, specify an available one instead. Make sure to also update your `sfdx-project.json` file by setting the oauthLocalPort property to the new port. For example, if you set the callback URL to `http://localhost:1919/OauthRedirect`:
```
"oauthLocalPort" : "1919"
```
7. (JWT only) Select **Use digital signatures**.
8. (JWT only) Click **Choose File** and upload the `server.crt` file that contains your digital certificate.
9. Add these OAuth scopes:
- **Access and manage your data (api)**
- **Perform requests on your behalf at any time (refresh_token, offline_access)**
- **Provide access to your data via the Web (web)**
10. Click **Save**.

> Important
> 
> Make note of the **consumer key** because you need it later when you run a `auth` command.

11. (JWT only) Click **Manage**.
12. (JWT only) Click **Edit Policies**.
13. (JWT only) In the OAuth Policies section, select **Admin approved users are pre-authorized** for permitted users, and click **OK**.
14. (JWT only) Click **Save**.
15. (JWT only) Click **Manage Profiles** and then click **Manage Permission Sets**. Select the profiles and permission sets that are pre-authorized to use this connected app. Create permission sets if necessary (e.g. create a new Permission Set **Salesforce DX** and assign to the user performing CI).

### 3. Bitbucket Repository Variables
Below variables will be used by pipeline script.

Setup via Bitbucket Repository - Repository settings - Repository variables.

Name | Description
- | -
DECRYPTION_KEY | The key obtained from running `openssl enc ... -P`
DECRYPTION_IV | The iv obtained from running `openssl enc ... -P`
HUB_CONSUMER_KEY | The consumer key of Connected App in Dev Hub Org
HUB_USER_NAME | The username of the user in Dev Hub Org who has access to the Connected App
DEV_CONSUMER_KEY | The consumer key of Connected App in Dev Sandbox Org
DEV_USER_NAME | The username of the user in Dev Sandbox Org who has access to the Connected App

### 4. Repository 
Can use this repository as the starting point, though:

- `assets/server.key.enc`: needs to be replaced with project specific one
- `bitbucekt-pipelines.yml`: needs to expand according to branching model, branch naming convention and project requirements
- `config/project-scratch-def.json`: needs to expand according to the project requirements
- `dev-tools-scripts/oft-init-scratch.sh`: needs to expand according to project

## Task: Try it yourself
1. Sign up a Developer Edition Org via https://developer.salesforce.com/signup
2. Follow steps in Create a Private Key and Self-Signed Digital Certificate to get `server.key.enc`, key and iv for decryption.
3. Follow steps in Create Connected App to get consumer key, grant a CI user access to the connected app.
4. Fork the bitbucket repository, enable pipeline, configure repository variables (use Dev Hub as the Dev for testing purpose), create a branch (type Other) named `develop`, create a feature branch (type Feature) named `import-server-key-enc` of `develop` branch.
5. Create a local directory, clone newly created bitbucket repository and checkout the branch `feature/import-server-key-enc`.

- Replace `assets/server.key.enc` with the one generated above.
- Depending on whether Sandbox or Developer Edition Org is used as DEV, update parameter for `--instanceurl` at around line 56 of `bitbucket-pipelines.yml`.

6. Commit changes and push to remote.
7. Create Pull Request from branch `feature/import-server-key-enc` to branch `develop`.

- Pipeline should start running on the Pull Request immediately.
- After merging the Pull Request, Pipeline should start running and deploy to DEV environment. 

8. Create a new branch of `develop`, fetch and checkout on your local machine.
9. Make sure that Dev Hub has been authorised with alias per `DEV_HUB_ALIAS` in `dev-tools-scripts/lib/oft-local-config.sh`.
9. Run script `dev-tools-scripts/oft-init-scratch.sh` to create a scratch org.
10. Make some changes to the scratch org and use `sfdx force:source:pull` to pull changes to local repository.
11. Repeat steps 6 & 7, changes will be deployed to DEV environment successfully.